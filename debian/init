#!/bin/sh
# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          mtail
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Extract monitoring data from logs
# Description:       mtail is a tool for extracting metrics from application
#                    logs to be exported into a timeseries database or
#                    timeseries calculator for alerting and dashboarding.
#                    It aims to fill a niche between applications that do not
#                    export their own internal state, and existing monitoring
#                    systems, without patching those applications or rewriting
#                    the same framework for custom extraction glue code.
#                    Metrics are exported for scraping by a collector as JSON
#                    or Prometheus format over HTTP, or can be periodically
#                    sent to a collectd, StatsD, or Graphite collector socket.
### END INIT INFO

# Author: Martina Ferrari <tina@debian.org>
# Author: Guillem Jover <gjover@sipwise.com>

DESC="monitoring data extractor from application logs"
NAME=mtail
USER=mtail
GROUP=$USER
DAEMON=/usr/bin/$NAME
PIDFILE=/run/$NAME/$NAME.pid
LOGFILE=/var/log/$NAME/$NAME.log

START_ARGS="--no-close --background --make-pidfile"
STOP_ARGS="--remove-pidfile"

ARGS="-progs /etc/mtail -logtostderr"
[ -n "$HOST" ] && ARGS="${ARGS} -address $HOST"
[ -n "$PORT" ] && ARGS="${ARGS} -port $PORT"

# Deprecated options, left for backwards-compatibility.
[ -n "$COLLECTD_SOCKETPATH" ] && \
    ARGS="${ARGS} -collectd_socketpath $COLLECTD_SOCKETPATH"
[ -n "$GRAPHITE_HOSTPORT" ] && \
    ARGS="${ARGS} -graphite_host_port $GRAPHITE_HOSTPORT"
[ -n "$STATSD_HOSTPORT" ] && ARGS="${ARGS} -statsd_hostport $STATSD_HOSTPORT"
[ -n "$METRIC_PUSH_INTERVAL" ] && \
    ARGS="${ARGS} -metric_push_interval_seconds $METRIC_PUSH_INTERVAL"

do_start_prepare()
{
  mkdir -p $(dirname $PIDFILE)
}

do_start_cmd_override()
{
  start-stop-daemon --start --quiet --oknodo \
    --exec $DAEMON --pidfile $PIDFILE --user $USER --group $GROUP \
    --chuid $USER:$GROUP $START_ARGS -- $ARGS -logs "$LOGS" $EXTRA_ARGS \
    >>$LOGFILE 2>&1
}

do_stop_cmd_override()
{
  start-stop-daemon --stop --quiet --oknodo --retry=TERM/30/KILL/5 \
    --exec $DAEMON --pidfile $PIDFILE --user $USER $STOP_ARGS
}

alias do_reload=do_reload_sigusr1
